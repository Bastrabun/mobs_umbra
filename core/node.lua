--[[
	Mobs Umbra - Adds a shadow-looking Non Playing Character.
	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Node definition and registration
--

minetest.register_node('mobs_umbra:obscure_node', {
	description = mobs_umbra.l10n("Umbra's Obscure Node"),
	groups = {not_in_creative_inventory = 1},
	drawtype = 'normal',
	sunlight_propagates = false,
	walkable = false,
	pointable = false,
	diggable = false,
	climbable = false,
	buildable_to = true,
	floodable = false,
	color = 'black',
	post_effect_color = 'black',

	on_construct = function(pos)
		minetest.get_node_timer(pos):start(mobs_umbra.darkNodeTimeout)
	end,

	on_timer = function(pos, elapsed)
		minetest.set_node(pos, {name = 'air'})

		return true
	end
})

if (mobs_umbra.darkCubeDamages == true) then
	minetest.override_item('mobs_umbra:obscure_node', {
		damage_per_second = mobs_umbra.darkCubeDPS,
	})
end
