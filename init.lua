--[[
	Mobs Umbra - Adds a shadow-looking Non Playing Character.
	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Global mod's namespace
--

mobs_umbra = {}


--
-- Constants
--

-- Used for localization
mobs_umbra.l10n = minetest.get_translator('mobs_umbra')

-- Spawner frequency, stated in seconds.
mobs_umbra.spawnInterval =
	tonumber(minetest.settings:get('mobs_umbra_spawn_interval')) or 60

-- Spawning chance; 1 = always, 2 = 50%, etc.
mobs_umbra.spawnChance =
	tonumber(minetest.settings:get('mobs_umbra_spawn_chance')) or 7500

-- Number of umbras per active mapchunk.
mobs_umbra.AOC = tonumber(minetest.settings:get('mobs_umbra_aoc')) or 1

-- Min spawn height, stated in nodes.
mobs_umbra.minHeight =
	tonumber(minetest.settings:get('mobs_umbra_min_height')) or -30912

-- Max spawn height, stated in nodes.
mobs_umbra.maxHeight =
	tonumber(minetest.settings:get('mobs_umbra_max_height')) or -300

-- Offset used to determine the cube of darkness' volume
mobs_umbra.darkCubeOffset =
	tonumber(minetest.settings:get('mobs_umbra_cube_offset')) or 2

-- Seconds until the cube of darkness vanishes
mobs_umbra.darkNodeTimeout =
	tonumber(minetest.settings:get('mobs_umbra_darkness_timeout')) or 20

-- Whether if the cube of darkness should damage
mobs_umbra.darkCubeDamages =
	minetest.settings:get_bool('mobs_umbra_darkness_damage') or false

-- How much damage per second should be dealt by the cube of darkness
mobs_umbra.darkCubeDPS =
	tonumber(minetest.settings:get('mobs_umbra_darkness_dps')) or 1


--
-- Procedures
--

-- Minetest logger
local pr_LogMessage = function()

	-- Constant
	local s_LOG_LEVEL = minetest.settings:get('debug_log_level')

	-- Body
	if (s_LOG_LEVEL == nil)
	or (s_LOG_LEVEL == 'action')
	or (s_LOG_LEVEL == 'info')
	or (s_LOG_LEVEL == 'verbose')
	then
		minetest.log('action', '[Mod] Mobs Umbra [v0.2.1] loaded.')
	end
end


-- Subfiles loader
local pr_LoadSubFiles = function()

	-- Constant
	local s_MOD_PATH = minetest.get_modpath('mobs_umbra')

	-- Body
	dofile(s_MOD_PATH .. '/core/functions.lua')
	dofile(s_MOD_PATH .. '/core/craft_item.lua')
	dofile(s_MOD_PATH .. '/core/node.lua')
	dofile(s_MOD_PATH .. '/core/projectile.lua')
	dofile(s_MOD_PATH .. '/core/mob.lua')
	dofile(s_MOD_PATH .. '/core/spawning.lua')

end


--
-- Main body
--

pr_LoadSubFiles()
pr_LogMessage()
